let checkExist = setInterval(function () {
    let passageCheck = document.getElementById('passage-start');

    if (document.contains(passageCheck)) {
        const disableComic = document.createElement('button');
        let comicCounter = true;
        disableComic.id = 'disable-id';
        disableComic.innerHTML = 'Click here if you want to disable custom CSS';

        disableComic.onclick = () => {
            const cssFile = 'style-imported-style-css';
            if (comicCounter) {
                document.getElementById(cssFile).disabled = true;
                comicCounter = false;
            } else {
                document.getElementById(cssFile).disabled = false;
                comicCounter = true;
            }
        }

        document.getElementById('passage-start').prepend(disableComic);
        clearInterval(checkExist);
    }
}, 1000);

window.onbeforeunload = function () {
    return "Do you really want to leave?";
};